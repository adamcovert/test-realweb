// import gulp from 'gulp';
// import svgSprite from 'gulp-svg-sprites';
// import plumber from 'gulp-plumber';
// import fs from 'fs';
//
// gulp.task('svg-sprite', () => {
//     gulp.src('./__dev/sprites/svg/*.svg')
//         .pipe(svgSprite({
//             cssFile: "../styles/helpers/08_svg-sprite.styl",
//             svg: {
//                 sprite: "../images/sprite-icons.svg"
//             },
//             svgPath: './images/sprite-icons.svg',
//             preview: false,
//             padding: 5,
//             templates: {
//                 css: fs.readFileSync(process.cwd() + '/__dev/styles/helpers/svg-sprite-template.css', "utf-8")
//             },
//             svgId: "svg-%f"
//         }))
//         .pipe(plumber())
//         .pipe(gulp.dest('./__dev/sprites'))
// });
//

import gulp from 'gulp';
import rename from 'gulp-rename';
import svgstore from 'gulp-svgstore';
import svgmin from 'gulp-svgmin';
import cheerio from 'gulp-cheerio';

gulp.task('svg-sprite', () => {
  return gulp.src('./__dev/sprites/svg/*.svg')
    .pipe(svgmin(function (file) {
      return {
        plugins: [{
          cleanupIDs: {
            minify: true
          }
        }]
      }
    }))
  .pipe(svgstore({
    inlineSvg: true
  }))
  .pipe(cheerio({
    run: function($) {
      $('svg').attr('style',  'display:none');
    },
    parserOptions: {
      xmlMode: true
    }
  }))
  .pipe(rename('sprite-svg.svg'))
  .pipe(gulp.dest('./__dev/images/'));
});