'use strict';
$(document).ready(function() {

    document.addEventListener('click', function() {

        if (window.Element && !Element.prototype.closest) {
            Element.prototype.closest =
                function(s) {
                    var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                        i,
                        el = this;
                    do {
                        i = matches.length;
                        while (--i >= 0 && matches.item(i) !== el) {};
                    } while (i < 0 && (el = el.parentElement));
                    return el;
                };
        }

        if (window.NodeList && !NodeList.prototype.forEach) {
            NodeList.prototype.forEach = function(callback, thisArg) {
                thisArg = thisArg || window;
                for (var i = 0; i < this.length; i++) {
                    callback.call(thisArg, this[i], i, this);
                }
            };
        }

        var getSiblings = function getSiblings(elem) {
            var siblings = [];
            var sibling = elem.parentNode.firstChild;
            for (; sibling; sibling = sibling.nextSibling) {
                if (sibling.nodeType !== 1 || sibling === elem) continue;
                siblings.push(sibling);
            }
            return siblings;
        };

        if (!event.target.classList.contains('field-text__change')) return;

        var closestParent = event.target.closest('.field-text');

        if (event.target.innerText == 'изменить') {
            event.target.innerText = 'сохранить';
            closestParent.classList.add('is-active');
        } else {
            event.target.innerText = 'изменить';
            closestParent.classList.remove('is-active');
        }

        var siblings = getSiblings(event.target);

        siblings.forEach(function(element) {
            if (!element.classList.contains('field-text__input-wrap')) return;

            var elemChildren = element.children;

            for (var i = 0; i < elemChildren.length; i++) {
                if (elemChildren[i].getAttribute('readonly')) {
                    elemChildren[i].removeAttribute('readonly');
                    elemChildren[i].classList.add('is-active');
                } else {
                    elemChildren[i].setAttribute('readonly', 'readonly');
                    elemChildren[i].classList.remove('is-active');
                }
            }
        });
    });

    document.addEventListener('click', function() {

        if (!event.target.classList.contains('field-radio__add-address-btn')) return;

        var content = document.querySelector(event.target.hash);
        if (!content) return;

        event.preventDefault();

        content.classList.add('additional-address--is-active');
        event.target.style.display = 'none';
    });

    document.addEventListener('click', function() {

        if (!event.target.classList.contains('additional-address__close-btn')) return;

        var address = document.querySelector('.additional-address');

        if (address.classList.contains('additional-address--is-active')) {
            address.classList.remove('additional-address--is-active');
            document.querySelector('.field-radio__add-address-btn').style.display = 'inline-block';
        }
    });

    var inputs = document.querySelectorAll('.additional-address input');

    for (var j = 0; j < inputs.length; j++) {

        inputs[j].addEventListener('input', function() {

            var createAddress = function createAddress() {
                var content = '' + inputs[0].value + ' ' + inputs[1].value + ' ' + inputs[2].value + ' ' + inputs[3].value + '';
                return content;
            };

            var total = createAddress();
        });
    }
});